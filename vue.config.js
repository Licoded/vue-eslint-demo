const path = require('path');
const pkg = require('./package.json');

const ENV = process.env.VUE_APP_ENV || 'dev';
if (process.env.IS_BUILD) {
  process.env.NODE_ENV = 'production';
}

const outputDir = path.resolve(__dirname, `./dist/${ENV.toUpperCase()}`, pkg.version);
const url = `https://example.com/${pkg.name}/${ENV.toUpperCase()}/${pkg.version}/`;
const publicPath = process.env.NODE_ENV === 'development' ? '/' : url;

module.exports = {
  outputDir,
  publicPath,
  configureWebpack: {
    // eslint-disable-next-line no-undefined
    devtool: ENV === 'dev' ? 'source-map' : undefined,
    entry: {
      app: './base/main.js',
    },
    resolve: {
      alias: {
        base: path.resolve(__dirname, './base'),
        src: path.resolve(__dirname, './src'),
        '@': path.resolve(__dirname, './src'),
        '#': path.resolve(__dirname, './src/components'),
      },
    },
  },
};
